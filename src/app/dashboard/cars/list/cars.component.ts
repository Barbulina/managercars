import { ITEMS_BY_PAGES } from './../../../shared/constants/constants';
import { CarService } from './../car.service';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { ICar } from './../car.interface';
import { Subscription } from 'rxjs';
import { FormGroup, FormControl } from '@angular/forms';
@Component({
  selector: 'app-cars',
  templateUrl: './cars.component.html',
  styleUrls: ['./cars.component.scss']
})
export class CarsComponent implements OnInit, OnDestroy {
  public canLoadMore: boolean;
  public cars: Array<ICar> = [];
  private carsSubscription: Subscription;
  public formFilter: FormGroup;
  private page: number;

  constructor(
    private carService: CarService
  ) { }

  ngOnInit() {
    console.log('CARS INIT');
    this.page = 0;
    this.canLoadMore = true;
    this.loadCards();
    this.createFormFilter();
  }

  ngOnDestroy() {
    this.carsSubscription.unsubscribe();
  }

  addToFavorites(car) {
    if (this.carService.isCarInFavorite(car.id)) {
      this.carService.removeFavorite(car);
    } else {
      this.carService.addFavortire(car);
    }
  }

  createFormFilter() {
    this.formFilter = new FormGroup({
      brand: new FormControl(),
      model: new FormControl(),
      price: new FormControl(),
      start: new FormControl(1),
      end: new FormControl(5)
    });

    this.formFilter.valueChanges.subscribe(res => {
      this.page = 0;
      this.cars = [];
      this.canLoadMore = true;
      this.carService.loadCards(this.page, res);
    });
  }

  isInFavorite(car: ICar): boolean {
    return this.carService.isCarInFavorite(car.id);
  }

  submit() {
    console.log('<<');

  }

  loadCards() {
    this.carService.cars$.subscribe(cars => {
      console.log('cars', cars);
      if (cars.length < ITEMS_BY_PAGES) {
        this.canLoadMore = false;
      }
      this.cars = this.cars.concat(cars);
    });
  }

  loadMoreCards() {
    this.page = this.page + 1;
    this.carService.loadCards(this.page, this.formFilter.value);
  }
}
