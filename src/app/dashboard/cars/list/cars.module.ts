import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CarsRoutingModule } from './cars-routing.module';
import { CarsComponent } from './cars.component';
import { SharedModule } from '../../../shared/modules/shared/shared.module';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    CarsRoutingModule
  ],
  exports: [CarsComponent],
  declarations: [CarsComponent]
})
export class CarsModule { }
