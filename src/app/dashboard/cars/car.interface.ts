export interface ICar {
  brand: string;
  id: number;
  image: string;
  model: string;
  price: number;
}

export interface ICarFavorite {
  brand: string;
  id: number;
  image: string;
  model: string;
}

export interface IFiltersList {
  brand: string;
  model: string;
  price: number;
}

