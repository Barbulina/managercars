import { URL_IMAGES, ITEMS_BY_PAGES } from './../../shared/constants/constants';
import { ICar, ICarFavorite, IFiltersList } from './car.interface';
import { Subject, Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
@Injectable({
  providedIn: 'root'
})
export class CarService {

  private carsSubject$: Subject<Array<ICar>> = new Subject();
  public cars$: Observable<Array<ICar>> = this.carsSubject$.asObservable();
  public carsFavorites: Array<ICarFavorite> = [];

  constructor(private http: HttpClient) {
    this.loadCards(0);
  }

  addFavortire(car: ICarFavorite) {
    const index = this.carsFavorites.findIndex(cardFavorite => cardFavorite.id === car.id);
    if (!this.isCarInFavorite(car.id)) {
      this.carsFavorites.push(car);
    }
  }

  isCarInFavorite(carId: number): boolean {
    const index = this.carsFavorites.findIndex(cardFavorite => cardFavorite.id === carId);
    if (index < 0) {
      return false;
    } else {
      return true;
    }
  }

  loadCards(page: number, filterParams?: IFiltersList): void {
    this.http.get('assets/data/cars.json')
      .subscribe((res: any) => {
        let cars = res;
        cars.map(car => car.image = `${URL_IMAGES}${car.image}`);
        if (filterParams && filterParams.brand) {
          cars = cars.filter(it => {
            return it.brand.toLowerCase().includes(filterParams.brand);
          });
        }
        if (filterParams && filterParams.model) {
          cars = cars.filter(it => {
            return it.model.toLowerCase().includes(filterParams.model);
          });
        }
        if (filterParams && filterParams.price) {
          cars = cars.filter(it => {
            return it.price.toLowerCase().includes(filterParams.price);
          });
        }
        let start = 0;
        if (page !== 0) {
          start = page * ITEMS_BY_PAGES;
        }
        const end = start + ITEMS_BY_PAGES;
        console.log('page: ' + page + ' start: ' + start + 'end: ' + end);

        cars = cars.slice(start, end);
        this.carsSubject$.next(cars);
      });
  }

  removeFavorite(car: ICarFavorite) {
    const index = this.carsFavorites.findIndex(cardFavorite => cardFavorite.id === car.id);
    if (index > -1) {
      this.carsFavorites.splice(index, 1);
    }
  }

}
