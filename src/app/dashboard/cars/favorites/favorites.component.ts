import { MatDialogRef } from '@angular/material';
import { CarService } from './../car.service';
import { ICarFavorite } from './../car.interface';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-favorites',
  templateUrl: './favorites.component.html',
  styleUrls: ['./favorites.component.scss']
})
export class FavoritesComponent implements OnInit {
  constructor(private carService: CarService, private dialogRef: MatDialogRef<FavoritesComponent>) { }
  public favorites: Array<ICarFavorite>;

  ngOnInit() {
    this.favorites = this.carService.carsFavorites;
  }

  remove(car: ICarFavorite) {
    this.carService.removeFavorite(car);
    if (this.favorites.length < 1) {
      this.dialogRef.close();
    }
  }

}
