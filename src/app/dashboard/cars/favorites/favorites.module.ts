import { SharedModule } from './../../../shared/modules/shared/shared.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FavoritesComponent } from './favorites.component';

@NgModule({
  imports: [
    CommonModule,
    SharedModule
  ],
  declarations: [FavoritesComponent],
  entryComponents: [FavoritesComponent]
})
export class FavoritesModule { }
