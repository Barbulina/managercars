import { FavoritesComponent } from './favorites.component';
import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { MatDialog, MatDialogRef, MatDialogConfig } from '@angular/material';

@Injectable({
  providedIn: 'root'
})
export class FavoriteDialogService {

  constructor(private dialog: MatDialog) { }

  open(): Observable<boolean> {
    let dialogRef: MatDialogRef<FavoritesComponent>;
    const config = new MatDialogConfig();
    dialogRef = this.dialog.open(FavoritesComponent, {
      width: '50%'
    });
    return dialogRef.afterClosed();
  }

}
