import { CarsModule } from './cars/list/cars.module';
import { SharedModule } from './../shared/modules/shared/shared.module';
import { FavoritesModule } from './cars/favorites/favorites.module';
import { DashboardComponent } from './dashboard.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DashboardRoutingModule } from './dashboard-routing.module';

@NgModule({
  imports: [
    CommonModule,
    DashboardRoutingModule,
    SharedModule,
    FavoritesModule,
    CarsModule
  ],
  declarations: [DashboardComponent]
})
export class DashboardModule { }
