import { ICarFavorite } from './cars/car.interface';
import { CarService } from './cars/car.service';
import { FavoriteDialogService } from './cars/favorites/favorite-dialog.service';
import { Component, OnInit } from '@angular/core';
import { BreakpointObserver, Breakpoints, BreakpointState } from '@angular/cdk/layout';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {

  public favorites: Array<ICarFavorite>;
  constructor(private carService: CarService, private favoriteDialogService: FavoriteDialogService) { }
  ngOnInit() {
    this.favorites = this.carService.carsFavorites;
  }
  openFavorites() {
    if (this.favorites.length > 0) {
      this.favoriteDialogService.open();
    }
  }
}
